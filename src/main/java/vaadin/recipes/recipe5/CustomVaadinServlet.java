package vaadin.recipes.recipe5;

import com.vaadin.flow.spring.SpringServlet;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;

import javax.servlet.ServletException;

@Slf4j
public class CustomVaadinServlet extends SpringServlet {
    /**
     * Creates a new Vaadin servlet instance with the application
     * {@code context} provided.
     *
     * @param context the Spring application context
     */
    public CustomVaadinServlet(ApplicationContext context) {
        super(context);
    }

    @Override protected void servletInitialized() throws ServletException {
        super.servletInitialized();
        log.info(this.getClass().getName() + " initialized.");
    }
}
