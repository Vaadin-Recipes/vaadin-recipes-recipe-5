package vaadin.recipes.recipe5autoconf;

import com.vaadin.flow.server.Constants;
import com.vaadin.flow.spring.RootMappedCondition;
import com.vaadin.flow.spring.SpringBootAutoConfiguration;
import com.vaadin.flow.spring.SpringServlet;
import com.vaadin.flow.spring.VaadinConfigurationProperties;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.ClassUtils;
import org.springframework.web.context.WebApplicationContext;
import vaadin.recipes.recipe5.CustomVaadinServlet;

import java.util.HashMap;
import java.util.Map;

@Configuration
@EnableConfigurationProperties(VaadinConfigurationProperties.class)
@AutoConfigureAfter(SpringBootAutoConfiguration.class)
public class CustomVaadinConfiguration {

    public static final String VAADIN_SERVLET_MAPPING = "/vaadinServlet/*";

    @Bean("servletRegistrationBean")
    public ServletRegistrationBean<CustomVaadinServlet> servletRegistrationBean(
            VaadinConfigurationProperties configurationProperties,
            WebApplicationContext context) {

        String mapping = configurationProperties.getUrlMapping();
        Map<String, String> initParameters = new HashMap<>();
        if (RootMappedCondition.isRootMapping(mapping)) {
            mapping = VAADIN_SERVLET_MAPPING;
            String url = mapping.replace("*", "");
            // / -> context://
            // foo -> context://foo
            // /foo -> context://foo
            if (url.startsWith("/")) {
                url = url.substring(1);
            }
            initParameters.put(Constants.SERVLET_PARAMETER_PUSH_URL,
                    "context://" + url);
        }
        ServletRegistrationBean<CustomVaadinServlet> registration = new ServletRegistrationBean<>(
                new CustomVaadinServlet(context), mapping);
        registration.setInitParameters(initParameters);
        registration
                .setAsyncSupported(configurationProperties.isAsyncSupported());
        registration.setName(
                ClassUtils.getShortNameAsProperty(SpringServlet.class));
        return registration;

    }
}
